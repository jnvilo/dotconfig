import os
from setuptools import setup

setup(
    name = "dotconfig",
    version = "0.0.1",
    author = "Jason Viloria",
    author_email = "jnvilo@gmail.com",
    description = ("A module for managing the ~/.config/app directory"),
    license = "BSD",
    keywords = "linux application configuration files",
    url = "https://bitbucket.org/jnvilo/dotconfig",
    install_requires=["requests","docopt","beautifulsoup4","sh"],
    packages=['dotconfig'],
    long_description="""This is a python module that can be used to access and manage the home directory .config """,
    #entry_points={
    #    'console_scripts': [
    #        'dotconfig = dotconfig:main',
    #    ]
    #},
 classifiers=[
          'Development Status :: 5 - Production/Stable',
          'Environment :: Console',
          'Intended Audience :: Developers',
          'Intended Audience :: System Administrators',
          'License :: OSI Approved :: BSD License',
          'Operating System :: MacOS :: MacOS X',
          'Operating System :: Unix',
          'Operating System :: POSIX',
          'Programming Language :: Python',
          'Programming Language :: Python :: 2.7',
          'Programming Language :: Python :: 3.2',
          'Programming Language :: Python :: 3.3',
          'Topic :: Software Development',
          'Topic :: Software Development :: Libraries',
          'Topic :: Software Development :: Libraries :: Python Modules',
    ],
)

